# Contributing Guide for [Wild Chat](https://gitlab.com/AWildBeard/WildChat)

#### General Contributions:
- If you have a suggestion, or error to report, open a [Issue](https://gitlab.com/AWildBeard/WildChat/issues/new?issue) and make it clear in the title as to what you want/ what the issue is etc.
- Don't be afraid to ask questions, or email me. Find my info [here](https://gitlab.com/AWildBeard)

#### Code/ Direct Contributions to the REPO:
- NEWLINE Style for code
- Fork, branch, test, then pull request is the recommended way to contribute ANY code/ artwork etc.
  - Remember to keep your fork up to date and ready to be merged on pull request submission.
