# Welcome!
![WildChat](https://gitlab.com/AWildBeard/resources/raw/master/WildChat/resources/wildChat.png)

#### [Downloads](https://gitlab.com/AWildBeard/WildChat/tags)

### The Project

This project is aimed at creating a good looking Twitch Chat client.
It adheres (as best to my ability) to the Twitch IRC specification
and does not include anything beyond that. (for now?)

##### Getting Started

- Check out the [QuickStart](https://gitlab.com/AWildBeard/WildChat/wikis/QuickStart) guide.

#### Usage

-- PLEASE NOTE THIS PROJECT IS CURRENTLY IN BETA --

Simply start the application, enter your credentials, then connect
to your favorite streamer's channel by using the connect button on the
top left. The program is fully themeable with the CustomizeUI button
on the menu bar, you can customize every color displayed in the
application. The application ships by default with a dark theme.

#### Contributing

- Please see [CONTRIBUTING.md](https://gitlab.com/AWildBeard/WildChat/blob/master/CONTRIBUTING.md)

#### TroubleShooting

- Check out the [TroubleShooting](https://gitlab.com/AWildBeard/WildChat/wikis/TroubleShooting) page first!
    - If that doesn't work, submit an [issue](https://gitlab.com/AWildBeard/WildChat/issues/new?issue).

